﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class TransformExtension
{
    /// <summary>
    /// Get all the children of the transform.
    /// </summary>
    /// <returns></returns>
    public static Transform[] GetAllChildren(this Transform target)
    {
        Transform[] children = new Transform[target.childCount];
        for (int i = 0; i < target.childCount; i++)
        {
            children[i] = target.GetChild(i);
        }
        return children;
    }


    /// <summary>
    /// Find a transform object by name, that is child of the target. 
    /// </summary>
    /// <param name="name"> The name of the transform I want to find. </param>
    /// <param name="type"> The type of search that I wat to use. </param>
    /// <returns></returns>
    public static Transform FindTransformByName(this Transform target, string name, TransformSearchType type = TransformSearchType.depthFirst)
    {
        Transform child = null;

        switch (type)
        {
            case TransformSearchType.depthFirst:
                child = FindTransformDepthFirst(target, name);
                break;
            case TransformSearchType.breadthFirst:
                child = FindTransformBreadthFirst(target, name);
                break;
        }

        return child;
    }

    /// <summary>
    /// Find a transform object by name, that is child of the target. 
    /// </summary>
    /// <param name="name"> The name of the transform I want to find. </param>
    /// <param name="type"> The type of search that I wat to use. </param>
    /// <returns></returns>
    public static Transform[] FindTransformByName(this Transform target, string[] names, TransformSearchType type = TransformSearchType.depthFirst)
    {
        Transform[] childrens = null;

        switch (type)
        {
            case TransformSearchType.depthFirst:
                childrens = FindTransformDepthFirst(target, names);
                break;
            case TransformSearchType.breadthFirst:
                childrens = FindTransformBreadthFirst(target, names);
                break;
        }

        return childrens;
    }

    /// <summary>
    /// Uses the Depth First algorithm to find the children.
    /// </summary>
    private static Transform FindTransformDepthFirst(Transform target, string name)
    {
        Transform child = null;
        Stack<Transform> list = new Stack<Transform>();
        list.Push(target);

        do
        {
            child = list.Get();

            if (child.name.Equals(name))
                return child;

            if (child.childCount > 0)
                list.AddAll<Transform>(child.GetAllChildren());

        }
        while (list.Count >= 0);

        return child;
    }


    /// <summary>
    /// Uses the Depth First algorithm to find the children.
    /// </summary>
    private static Transform[] FindTransformDepthFirst(Transform target, string[] names)
    {
        Transform child = null;
        Stack<Transform> list = new Stack<Transform>();
        List<Transform> children = new List<Transform>();
        List<string> namesList = new List<string>();

        namesList.AddRange(names);
        list.Push(target);

        do
        {
            child = list.Get();

            if (child.name.EqualsToList(namesList))
            {
                children.Add(child);
                namesList.Remove(child.name);
            }

            if (child.childCount > 0)
                list.AddAll<Transform>(child.GetAllChildren());

        }
        while (list.Count >= 0 && namesList.Count > 0);

        return children.ToArray();
    }


    /// <summary>
    /// Uses the Breadth First algorithm to find the children.
    /// </summary>
    private static Transform FindTransformBreadthFirst(Transform target, string name)
    {
        Transform child;
        Queue<Transform> list = new Queue<Transform>();
        list.Enqueue(target);

        do
        {
            child = list.Get();

            if (child.name.Equals(name))
                return child;

            if (child.childCount > 0)
                list.AddAll(child.GetAllChildren());

        }
        while (list.Count >= 0);

        return child;
    }

    /// <summary>
    /// Uses the Breadth First algorithm to find the children.
    /// </summary>
    private static Transform[] FindTransformBreadthFirst(Transform target, string[] names)
    {
        Transform child;
        List<Transform> children = new List<Transform>();
        Queue<Transform> list = new Queue<Transform>();
        List<string> namesList = new List<string>();

        namesList.AddRange(names);
        list.Enqueue(target);

        do
        {
            child = list.Get();

            if (child.name.EqualsToList(namesList))
            {
                children.Add(child);
                namesList.Remove(child.name);
            }

            if (child.childCount > 0)
                list.AddAll(child.GetAllChildren());

        }
        while (list.Count >= 0 && namesList.Count > 0);

        return children.ToArray();
    }


    public enum TransformSearchType
    {
        depthFirst,
        breadthFirst,
    }
}