﻿using UnityEngine;

public static class MonoBehaviourExtension
{
    /// <summary>
    /// Adds a component of Type: T to the GameObject only if there isn't a previous one of that type. 
    /// </summary>
    /// <typeparam name="T"> Type of component that is going to be added. </typeparam>
    /// <param name="target"> The GameObject instance that call the method. </param>
    /// <returns></returns>
    public static T AddUniqueComponent<T>(this GameObject target) where T : Component
    {
        var targetComponent = target.GetComponent<T>();

        if (targetComponent == null)
        {
            targetComponent = target.AddComponent<T>();
        }
        else
        {
            DebugDev.Log($"There is already a {typeof(T).ToString().Bold()} component in {target.name.Bold()}");
        }

        return targetComponent;
    }
}