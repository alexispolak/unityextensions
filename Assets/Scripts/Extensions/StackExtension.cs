﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class StackExtension
{
    /// <summary>
    /// Pushing all the objects of the array.
    /// </summary>
    /// <typeparam name="T"> Type of the array. </typeparam>
    /// <param name="array"> The array of object pushing to the stack. </param>
    /// <returns></returns>
    public static Stack<T> AddAll<T>(this Stack<T> stack, T[] array)
    {
        for (int i = 0; i < array.Length; i++)
        {
            stack.Push(array[i]);
        }

        return stack;
    }

    /// <summary>
    /// Pushing all the objects of the list.
    /// </summary>
    /// <typeparam name="T"> Type of the list. </typeparam>
    /// <param name="list"> The list of object pushing to the stack. </param>
    /// <returns></returns>
    public static Stack<T> AddAll<T>(this Stack<T> stack, List<T> list)
    {
        for (int i = 0; i < list.Count; i++)
        {
            stack.Push(list[i]);
        }

        return stack;
    }

    /// <summary>
    /// Pop an item from the stack.
    /// </summary>
    /// <typeparam name="T"> Type of the returned object. </typeparam>
    /// <returns></returns>
    public static T Get<T>(this Stack<T> stack)
    {
        return stack.Pop();
    }
}
