﻿using System;
using System.Collections.Generic;

public static class ListExtension
{
    /// <summary>
    /// Shuffle the list based on a rng.
    /// </summary>
    public static void Shuffle<T>(this List<T> list)
    {
        Random rng = new Random();

        int n = list.Count;
        while (n > 1)
        {
            n--;
            int k = rng.Next(n + 1);
            T value = list[k];
            list[k] = list[n];
            list[n] = value;
        }
    }

    /// <summary>
    /// Remove all the objets that are in the list recieved by parameter.
    /// </summary>
    /// <param name="toRemoveList"> The objects that are going to be removed. </param>
    public static void RemoveList<T>(this List<T> list, List<T> toRemoveList)
    {
        for (int i = 0; i < toRemoveList.Count; i++)
        {
            if(list.Contains(toRemoveList[i]))
                list.Remove(toRemoveList[i]);
        }
    }

    /// <summary>
    /// Add all the objets that are in the list recieved by parameter.
    /// </summary>
    /// <param name="toAddList"> The objects that are going to be added. </param>
    public static void AddList<T>(this List<T> list, List<T> toAddList)
    {
        for (int i = 0; i < toAddList.Count; i++)
        {
            list.Add(toAddList[i]);
        }
    }

    /// <summary>
    /// Creates a new list with all the objects cloned.
    /// </summary>
    /// <returns> A list that all of the objects are new clone isntances of the list. </returns>
    public static List<T> CloneList<T>(this List<T> list) where T : ICloneable
    {
        List<T> newList = new List<T>();
        for (int i = 0; i < list.Count; i++)
        {
            newList.Add((T)list[i].Clone());
        }
        return newList;
    }

    /// <summary>
    /// Get the first element of the list.
    /// </summary>
    public static T GetFirst<T>(this List<T> list)
    {
        return list[0];
    }

    /// <summary>
    /// Get the last element of the list
    /// </summary>
    public static T GetLast<T>(this List<T> list)
    {
        return list[list.Count - 1];
    }

    /// <summary>
    /// Remove the first element of the list.
    /// </summary>
    public static void RemoveFirst<T>(this List<T> list)
    {
        list.Remove(list[0]);
    }

    /// <summary>
    /// Remove the last element of the list.
    /// </summary>
    public static void RemoveLast<T>(this List<T> list)
    {
        list.Remove(list[list.Count - 1]);
    }

    /// <summary>
    /// Get the index of the element in the middle of the list.
    /// </summary>
    public static int GetMidIndex<T>(this List<T> list)
    {
        return Decimal.ToInt32(list.Count / 2);
    }

    /// <summary>
    /// Split the list into two parts using the "position" as the splitting point.
    /// </summary>
    /// <param name="leftPart"> The left part of the list. </param>
    /// <param name="rightPart"> The right part of the list. </param>
    /// <param name="position"> The position of the original list where the split is goint to happen. </param>
    /// <param name="exclusive"> If the split is on the position, or in the next index. </param>
    public static void Split<T>(this List<T> list, ref List<T> leftPart, ref List<T> rightPart, int position = -1, bool exclusive = true)
    {
        //Save the middle position if not recieved by parameter.
        if (position == -1)
            position = list.GetMidIndex<T>();

        //Lazy initialization of the list if null
        if (leftPart == null)
            leftPart = new List<T>();
        if (rightPart == null)
            rightPart = new List<T>();

        int exclusiveMod = 0;
        if (exclusive)
            exclusiveMod = 1;

        for (int i = 0; i < list.Count; i++)
        {
            if (i < position + exclusiveMod)
                leftPart.Add(list[i]);
            else
                rightPart.Add(list[i]);
        }
    }
}
