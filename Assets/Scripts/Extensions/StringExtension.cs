﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;

public static class StringExtension
{
    /// <summary>
    /// Caesar Cipher encoding.
    /// </summary>
    /// <param name="shift"> The amount of positions every letter is going to shif. </param>
    /// <returns> The encoded string. </returns>
    public static string Caesar(this string source, int shift)
    {
        var maxChar = Convert.ToInt32(char.MaxValue);
        var minChar = Convert.ToInt32(char.MinValue);

        var buffer = source.ToCharArray();

        for (var i = 0; i < buffer.Length; i++)
        {
            var shifted = Convert.ToInt32(buffer[i]) + shift;

            if (shifted > maxChar)
            {
                shifted -= maxChar;
            }
            else if (shifted < minChar)
            {
                shifted += maxChar;
            }

            buffer[i] = Convert.ToChar(shifted);
        }

        return new string(buffer);
    }

    /// <summary>
    /// Caesar Cipher decoding.
    /// </summary>
    /// <param name="shift">  The amount of positions every letter is going to shif. </param>
    /// <returns> The decoded string. </returns>
    public static string UnCaesar(this string source, int shift)
    {
        return source.Caesar(shift*-1);
    }


    public static bool EqualsToArray(this string stringToCheck, string[] strings)
    {
        for (int i = 0; i < strings.Length; i++)
        {
            if (stringToCheck.Equals(strings[i]))
                return true;
        }

        return false;
    }

    public static bool EqualsToList(this string stringToCheck, List<string> strings)
    {
        for (int i = 0; i < strings.Count; i++)
        {
            if (stringToCheck.Equals(strings[i]))
                return true;
        }

        return false;
    }

    /// <summary>
    /// Creates a richText colored string for the console.
    /// </summary>
    /// <param name="boringString"> The string that is going to be colored. </param>
    /// <param name="color"> The color. </param>
    /// <returns> A Rich Text string formated to be display on the selected color. </returns>
    public static string Colored(this string boringString, Color color)
    {
        return $"<color={color.GetName()}>{boringString}</color>";
    }

    /// <summary>
    /// Creates a RichText bold string for the console.
    /// </summary>
    /// <param name="boringString"> The string that is going to be colored. </param>
    /// <returns> A Rich Text string formated to be bold. </returns>
    public static string Bold(this string boringString)
    {
        return $"<b>{boringString}</b>";
    }
}