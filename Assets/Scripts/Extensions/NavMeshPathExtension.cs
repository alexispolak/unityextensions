﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public static class NavMeshPathExtension
{
    /// <summary>
    /// Gets the full length of the calculated path
    /// </summary>
    public static float Distance(this NavMeshPath path)
    {

        float sum = 0f;

        if (path.corners.Length < 2) return sum;

        for (int i = 1; i < path.corners.Length; i++)
        {
            sum += Vector3.Distance(path.corners[i], path.corners[i - 1]);
        }

        return sum;
    }
}