﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class QueueExtension
{
    /// <summary>
    /// Enqueue all the objects of the array.
    /// </summary>
    /// <typeparam name="T"> Type of the array. </typeparam>
    /// <param name="array"> The array of object enqueueing to the queue. </param>
    /// <returns></returns>
    public static Queue<T> AddAll<T>(this Queue<T> queue, T[] array)
    {
        for (int i = 0; i < array.Length; i++)
        {
            queue.Enqueue(array[i]);
        }

        return queue;
    }

    /// <summary>
    /// Enqueue all the objects of the list.
    /// </summary>
    /// <typeparam name="T"> Type of the list. </typeparam>
    /// <param name="list"> The list of object enqueueing to the queue. </param>
    /// <returns></returns>
    public static Queue<T> AddAll<T>(this Queue<T> queue, List<T> list)
    {
        for (int i = 0; i < list.Count; i++)
        {
            queue.Enqueue(list[i]);
        }

        return queue;
    }

    /// <summary>
    /// Deque an item from the queue.
    /// </summary>
    /// <typeparam name="T"> Type of the returned object. </typeparam>
    /// <returns></returns>
    public static T Get<T>(this Queue<T> queue)
    {
        return queue.Dequeue();
    }
}

