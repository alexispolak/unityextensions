﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class Vector3Extension
{
    /// <summary>
    /// Returns if the vector is equal to the recieved by paramenter based on a threshold.
    /// </summary>
    /// <param name="vectorToCompare"> The vector that is going to be compared. </param>
    /// <param name="threshold"> The threshold of the comparison. </param>
    /// <returns></returns>
    public static bool Equal(this Vector3 a, Vector3 vectorToCompare, float threshold = 0.005f)
    {
        var neg = -1;
        var x = a.x - vectorToCompare.x;
        var y = a.y - vectorToCompare.y;
        var z = a.z - vectorToCompare.z;

        return (x <= threshold && x >= threshold * neg) && 
               (y <= threshold && y >= threshold * neg) &&
               (z <= threshold && z >= threshold * neg);
    }

    /// <summary>
    /// Returns a SerializableVector3 that can be serialized.
    /// </summary>
    public static SerializableVector3 Serializable(this Vector3 vector)
    {
        return new SerializableVector3(vector.x, vector.y, vector.z);
    }

    /// <summary>
    /// Get the value of the SerializableVector3 into the Vector3.
    /// </summary>
    /// <param name="serializedVector"> The vector that is going to be deserialized. </param>
    public static void Unserialize(this Vector3 vector3, SerializableVector3 serializedVector)
    {
        vector3.x = serializedVector.x;
        vector3.y = serializedVector.y;
        vector3.z = serializedVector.z;
    }
}
