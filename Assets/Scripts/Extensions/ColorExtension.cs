﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class ColorExtension
{
    /// <summary>
    /// Only support the colors that the struct Color has predefined. Doesn't support clear color.
    /// </summary>
    /// <param name="color"></param>
    /// <returns></returns>
   public static string GetName(this Color color)
   {
        if (color.g >= 0.91f && color.g < 1f)
            return "yellow";

        if (color.r == 0)
        {
            if (color.g == 0)
            {
                if (color.b == 0)
                    return "black";
                else
                    return "blue";
            }
            else if (color.g == 1)
            {
                if (color.b == 0)
                    return "green";
                else
                    return "cyan";
            }
        }
        else if (color.r == 1)
        {
            if (color.g == 0)
            {
                if (color.b == 0)
                    return "red";
                else
                    return "magenta";
            }
            else if (color.g == 1)
            {
                if (color.b == 1)
                    return "white";
            }
        }
        else if (color.r == 0.5f)
            return "gray";

        DebugDev.ColorLog($"Color {color} not accepted", Color.red);
        return "clear";
    }
}
