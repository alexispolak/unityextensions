﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Test : MonoBehaviour
{
    void Start()
    {
        this.MonoBehaviourExtensionTest();

        this.DebugDevToolTest();

        this.Vector3ExtensionTest();

        this.ListExtensionTest();

        this.StringExtensionTest();
    }


    private void MonoBehaviourExtensionTest()
    {    
        DebugDev.Log("<b><color=red>MonoBehaviourExtensionTest()</color></b>");

        //Test MonoBehaviourExtension
        this.gameObject.AddUniqueComponent<Rigidbody>();
        this.gameObject.AddUniqueComponent<Rigidbody>();
    }

    private void DebugDevToolTest()
    {
        DebugDev.Log("<b><color=red>DebugDevToolTest()</color></b>");

        //Test DebugDev Tool
        DebugDev.ColorLog($"Color {Color.yellow.GetName().Bold()} log test", Color.yellow);
        DebugDev.ColorLog($"Color {Color.gray.GetName().Bold()} log test", Color.gray);
        DebugDev.ColorLog($"Color {Color.blue.GetName().Bold()} log test", Color.blue);
        DebugDev.ColorLog($"Color {Color.green.GetName().Bold()} log test", Color.green);
        DebugDev.ColorLog($"Color {Color.magenta.GetName().Bold()} log test", Color.magenta);
        DebugDev.ColorLog($"Color {Color.cyan.GetName().Bold()} log test", Color.cyan);
        DebugDev.ColorLog($"Color {Color.white.GetName().Bold()} log test", Color.white);
        DebugDev.ColorLog($"Color {Color.black.GetName().Bold()} log test", Color.black);
        DebugDev.Log("Log test");


        DebugDev.DevColorLog("dev Color log test", Color.red);
        DebugDev.DevLog("dev Log test");
    }

    private void Vector3ExtensionTest()
    {
        DebugDev.Log("<b><color=red>Vector3ExtensionTest()</color></b>");

        //Vector 3 comparison Test
        Vector3 a = new Vector3(0.345f, 0.345f, 0.345f);
        Vector3 b = new Vector3(0.345f, 0.345f, 0.346f);

        DebugDev.Log(a.Equal(b, 0.01f));
        DebugDev.Log(a.Equal(b, 0.0001f));
        DebugDev.Log(a.Equal(b, 0.001f));
    }


    private void ListExtensionTest()
    {
        DebugDev.Log("<b><color=red>ListExtensionTest()</color></b>");

        List<int> listA = new List<int>();
        List<int> listB = new List<int>();
        listA.AddRange(new[] { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9 });
        listB.AddRange(new[] { 0, 1, 2, 3, 4, 5, 6, 7, 8 });
        DebugDev.Log("List A: <b>0, 1, 2, 3, 4, 5, 6, 7, 8, 9</b>");
        DebugDev.Log("List B: <b>0, 1, 2, 3, 4, 5, 6, 7, 8</b>");

        //Test get middle index
        var middleA = listA.GetMidIndex();
        var middleB = listB.GetMidIndex();
        DebugDev.Log($"Middle A: {middleA.ToString().Colored(Color.blue).Bold()} Middle B: {middleB.ToString().Colored(Color.blue).Bold()}");

        //Test spliting
        List<int> Aleft = new List<int>();
        List<int> Aright = new List<int>();
        List<int> Bleft = new List<int>();
        List<int> Bright = new List<int>();

        listA.Split(ref Aleft, ref Aright);
        listB.Split(ref Bleft, ref Bright);
        DebugDev.Log($"A left count: {Aleft.Count.ToString().Colored(Color.blue).Bold()} last element: {Aleft.GetLast().ToString().Colored(Color.blue).Bold()}");
        DebugDev.Log($"A right count: {Aright.Count.ToString().Colored(Color.blue).Bold()} first element: {Aright.GetFirst().ToString().Colored(Color.blue).Bold()}");
        DebugDev.Log($"B left count: {Bleft.Count.ToString().Colored(Color.blue).Bold()} last element: {Bleft.GetLast().ToString().Colored(Color.blue).Bold()}");
        DebugDev.Log($"B right count: {Bright.Count.ToString().Colored(Color.blue).Bold()} first element: {Bright.GetFirst().ToString().Colored(Color.blue).Bold()}");

        //Test spliting with param
        Aleft.Clear();
        Aright.Clear();
        Bleft.Clear();
        Bright.Clear();

        listA.Split(ref Aleft, ref Aright, 3, true);
        listB.Split(ref Bleft, ref Bright, 3, true);
        DebugDev.Log($"A left count: {Aleft.Count.ToString().Colored(Color.blue).Bold()} last element: {Aleft.GetLast().ToString().Colored(Color.blue).Bold()}");
        DebugDev.Log($"A right count: {Aright.Count.ToString().Colored(Color.blue).Bold()} first element: {Aright.GetFirst().ToString().Colored(Color.blue).Bold()}");
        DebugDev.Log($"B left count: {Bleft.Count.ToString().Colored(Color.blue).Bold()} last element: {Bleft.GetLast().ToString().Colored(Color.blue).Bold()}");
        DebugDev.Log($"B right count: {Bright.Count.ToString().Colored(Color.blue).Bold()} first element: {Bright.GetFirst().ToString().Colored(Color.blue).Bold()}");

        //Test spliting with param
        Aleft.Clear();
        Aright.Clear();
        Bleft.Clear();
        Bright.Clear();

        listA.Split(ref Aleft, ref Aright, 3, false);
        listB.Split(ref Bleft, ref Bright, 3, false);
        DebugDev.Log($"A left count: {Aleft.Count.ToString().Colored(Color.blue).Bold()} last element: {Aleft.GetLast().ToString().Colored(Color.blue).Bold()}");
        DebugDev.Log($"A right count: {Aright.Count.ToString().Colored(Color.blue).Bold()} first element: {Aright.GetFirst().ToString().Colored(Color.blue).Bold()}");
        DebugDev.Log($"B left count: {Bleft.Count.ToString().Colored(Color.blue).Bold()} last element: {Bleft.GetLast().ToString().Colored(Color.blue).Bold()}");
        DebugDev.Log($"B right count: {Bright.Count.ToString().Colored(Color.blue).Bold()} first element: {Bright.GetFirst().ToString().Colored(Color.blue).Bold()}");
    }

    private void StringExtensionTest()
    {
        string a = "Caesar Cipher";
        DebugDev.Log("Caesar encoding");
        DebugDev.Log(a.Caesar(0));
        DebugDev.Log(a.Caesar(1));
        DebugDev.Log(a.Caesar(2));
        DebugDev.Log(a.Caesar(3));
        DebugDev.Log(a.Caesar(4));

        DebugDev.Log("Caesar decoding");
        DebugDev.Log(a.Caesar(0).UnCaesar(0));
        DebugDev.Log(a.Caesar(1).UnCaesar(1));
        DebugDev.Log(a.Caesar(2).UnCaesar(2));
        DebugDev.Log(a.Caesar(3).UnCaesar(3));
        DebugDev.Log(a.Caesar(4).UnCaesar(4));
    }
}
