﻿public interface ISerializableVector3
{
    float SqrMagnitude();
    float Magnitude();
    SerializableVector3 Normalized();
    void Normalize();
    bool Equal(SerializableVector3 vectorToCompare, float threshold = 0.005f);
    string ToString();
}
