# DebugDev Tool

## Description

Logs a message to the Unity Console. Works really well with the `StringExtension.cs` of the library.

Uses the Scripting Define Symbols to activate and deactivate all the logs if you need it. You have to add `DEV_LOG` to the configuration ([how to add it](https://docs.unity3d.com/Manual/PlatformDependentCompilation.html)) and use the `DebugDev.DevLog()` method.

At this moment, it uses `Debug.Log()` internally to show the content of the string, therefore it has the same features (*concatenation of strings, Rich Text, context argument, etc.*) and more features that `Debug.Log(` doesn't have. If you need the `Debug.Log()` documentation, you can find it [here.](https://docs.unity3d.com/ScriptReference/Debug.Log.html)

## Dependencies

- UnityEngine
- System.Diagnostics
- UnityEngine.Debug.Log()
- StringExtension.cs
- ColorExtension.cs

## Methods

### DebugDev.Log()

```c#
public static void Log(object message);
public static void Log(object message, Object context);
```

| Parameters |                                                              |
| ---------- | ------------------------------------------------------------ |
| context    | Object to which the message applies.                         |
| message    | String or object to be converted to string representation for display. |

**Description**

It's a simple wrapper of the Debug.Log() method of Unity. More info in the official documentation [page](https://docs.unity3d.com/ScriptReference/Debug.Log.html).

------

### DebugDev.ColorLog()

```c#
public static void ColorLog(object message, Color color);
public static void ColorLog(object message, Object context, Color color);
```

| Parameters |                                                              |
| ---------- | ------------------------------------------------------------ |
| context    | Object to which the message applies.                         |
| message    | String or object to be converted to string representation for display. |
| color      | The color that the string is going to have                   |

**Description**

Logs a message to the Unity Console using DebugDev.Log() and the Rich Text markup documented [here](https://docs.unity3d.com/Manual/StyledText.html).

------

### DebugDev.DevLog()

```c#
[Conditional("DEV_LOG")]
public static void DevLog(object message);
[Conditional("DEV_LOG")]
public static void DevLog(object message, Object context);
```

| Parameters |                                                              |
| ---------- | ------------------------------------------------------------ |
| context    | Object to which the message applies.                         |
| message    | String or object to be converted to string representation for display. |

**Description**

Logs a message to the Unity Console using DebugDev.Log() only if the `DEV_LOG` is added in the configuration of Scripting Define Symbols.

------

### DebugDev.ColorLog()

```c#
[Conditional("DEV_LOG")]
public static void DevColorLog(object message, Color color);
[Conditional("DEV_LOG")]
public static void DevColorLog(object message, Object context, Color color);       
```

| Parameters |                                                              |
| ---------- | ------------------------------------------------------------ |
| context    | Object to which the message applies.                         |
| message    | String or object to be converted to string representation for display. |
| color      | The color that the string is going to have                   |

**Description**

Logs a message to the Unity Console using DebugDev.Log() and the Rich Text markup documented [here](https://docs.unity3d.com/Manual/StyledText.html), only if the `DEV_LOG` is added in the configuration of Scripting Define Symbols.

------

## Examples

```c#
 // Show the message in the Unity Console.             
 DebugDev.Log("Hello world !");

 // Show the red colored message in  the Unity Console.
 DebugDev.ColorLog("Hello world !", Color.red);

 //Shows message if the DEV_LOG symbol is added.
 DebugDev.DevLog("Hello world !");

 //Shows message in color if the DEV_LOG symbol is added.
 DebugDev.DevColorLog("Hello world !", Color.red);           
```

## Roadmap

- [ ] Remove the UnityEngine.Debug.Log() dependency injecting the dependency by parameter. 

