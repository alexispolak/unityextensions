# SerializableVector3 

## Description

Representation of 3D vectors and points in a class that can be serialized to a database or file. It's not a replace for Vector3 struct, it's just a way to save the Vector3 data. Works really well with the `Vector3Extension.cs` of the library.

It has the most important functionality of the Vector3 struct (*default vectors, SqrMagnitude(), Magnitude(), operator overload, Normalize(), Equal(), etc*) and you can transform the Vector3 into a SerializableVector3 and vice versa using the `Vector3Extension.cs` extension methods.

## Dependencies

- System

## Properties

| Properties |                            |
| ---------- | -------------------------- |
| x          | X component of the vector. |
| y          | Y component of the vector. |
| z          | Z component of the vector. |

## Static Properties

| Properties       |                                                              |
| ---------------- | ------------------------------------------------------------ |
| up               | returns a new SerializableVector3 with values (0, 1, 0)      |
| down             | returns a new SerializableVector3 with values (0, -1, 0)     |
| forward          | returns a new SerializableVector3 with values (0, 0, 1)      |
| back             | returns a new SerializableVector3 with values (0, 0, -1)     |
| right            | returns a new SerializableVector3 with values (1, 0, 0)      |
| left             | returns a new SerializableVector3 with values (-1, 0, 0)     |
| one              | returns a new SerializableVector3 with values (1, 1, 1)      |
| zero             | returns a new SerializableVector3 with values (0, 0, 0)      |
| negativeInfinity | returns a new SerializableVector3 with values (float.negativeInfinity, float.negativeInfinity, float.negativeInfinity) |
| positiveInfinity | returns a new SerializableVector3 with values (float.positiveInfinity, float.positiveInfinity, float.positiveInfinity) |

## Constructors

| Constructor                                    |                                                         |
| ---------------------------------------------- | ------------------------------------------------------- |
| SerializableVector3()                          | returns a new SerializableVector3 with values (0, 0, 0) |
| SerializableVector3(float x, float y)          | returns a new SerializableVector3 with values (x, y, 0) |
| SerializableVector3(float x, float y, float z) | returns a new SerializableVector3 with values (x, y, z) |

## Public Methods

| Method                                                       |                                                              |
| ------------------------------------------------------------ | ------------------------------------------------------------ |
| SqrMagnitude()                                               | Returns the Magnitude without the square root calculus. It's a faster way to calculate the Magnitude but it's less precise. |
| Magnitude()                                                  | Returns the magnitude of the vector.                         |
| Normalized()                                                 | Returns a new vector normalized.                             |
| Normalize()                                                  | Normalize the vector.                                        |
| Equal(SerializableVector3 vectorToCompare, float threshold = 0.005f) | Returns if the vector is equal to the received by parameter based on a threshold. |
| ToString()                                                   | Returns a string representation of the object.               |

## Operators

| Operator    |                                                      |
| ----------- | ---------------------------------------------------- |
| operator +  | Adds two vectors.                                    |
| operator -  | Subtracts one vector from another.                   |
| operator *  | Multiplies a vector by a number.                     |
| operator /  | Divides a vector by a number.                        |
| operator == | Returns true if two vectors are approximately equal. |
| operator != | Returns true if vectors different.                   |

## Roadmap

- [ ] Add more content from the Vectro3 struct. 