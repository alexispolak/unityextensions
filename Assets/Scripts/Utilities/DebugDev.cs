﻿using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System;
using UnityEngine;

public class DebugDev
{
    private static Action<object> logEvent = delegate { UnityEngine.Debug.LogError("DebugDev --> logEvent not Implemented."); };

    #region DEV LOG

    #region Log
    [Conditional("DEV_LOG")]
    public static void DevLog(object message)
    {
        Log(message);
    }

    [Conditional("DEV_LOG")]
    public static void DevLog(object message, UnityEngine.Object context)
    {
        Log(message, context);
    }

    [Conditional("DEV_LOG")]
    public static void DevColorLog(object message, Color color)
    {
        string coloredMessage = message.ToString().Colored(color);
        Log(coloredMessage);
    }

    [Conditional("DEV_LOG")]
    public static void DevColorLog(object message, UnityEngine.Object context, Color color)
    {
        string coloredMessage = message.ToString().Colored(color);
        Log(coloredMessage, context);
    }
    #endregion

    #region Warning
    [Conditional("DEV_LOG")]
    public static void DevLogWarning(object message)
    {
        UnityEngine.Debug.LogWarning(message);
    }

    [Conditional("DEV_LOG")]
    public static void DevLogWarning(object message, UnityEngine.Object context)
    {
        UnityEngine.Debug.LogWarning(message, context);
    }

    [Conditional("DEV_LOG")]
    public static void DevColorLogWarning(object message, Color color)
    {
        string coloredMessage = message.ToString().Colored(color);
        LogWarning(coloredMessage);
    }

    [Conditional("DEV_LOG")]
    public static void DevColorLogWarning(object message, UnityEngine.Object context, Color color)
    {
        string coloredMessage = message.ToString().Colored(color);
        LogWarning(coloredMessage, context);
    }
    #endregion

    #region Error
    [Conditional("DEV_LOG")]
    public static void DevLogError(object message)
    {
        UnityEngine.Debug.LogError(message);
    }

    [Conditional("DEV_LOG")]
    public static void DevLogError(object message, UnityEngine.Object context)
    {
        UnityEngine.Debug.LogError(message, context);
    }

    [Conditional("DEV_LOG")]
    public static void DevColorLogError(object message, Color color)
    {
        string coloredMessage = message.ToString().Colored(color);
        LogError(coloredMessage);
    }

    [Conditional("DEV_LOG")]
    public static void DevColorLogError(object message, UnityEngine.Object context, Color color)
    {
        string coloredMessage = message.ToString().Colored(color);
        LogError(coloredMessage, context);
    }
    #endregion

    #endregion

    #region UNITY LOG

    #region Log
    public static void Log(object message)
    {
        UnityEngine.Debug.Log(message);
    }

    public static void Log(object message, UnityEngine.Object context)
    {
        UnityEngine.Debug.Log(message, context);
    }

    public static void ColorLog(object message, Color color)
    {
        string coloredMessage = message.ToString().Colored(color);
        Log(coloredMessage);
    }

    public static void ColorLog(object message, UnityEngine.Object context, Color color)
    {
        string coloredMessage = message.ToString().Colored(color);
        Log(coloredMessage, context);
    }
    #endregion

    #region Warning
    public static void LogWarning(object message)
    {
        UnityEngine.Debug.LogWarning(message);
    }

    public static void LogWarning(object message, UnityEngine.Object context)
    {
        UnityEngine.Debug.LogWarning(message, context);
    }

    public static void ColorLogWarning(object message, Color color)
    {
        string coloredMessage = message.ToString().Colored(color);
        LogWarning(coloredMessage);
    }

    public static void ColorLogWarning(object message, UnityEngine.Object context, Color color)
    {
        string coloredMessage = message.ToString().Colored(color);
        LogWarning(coloredMessage, context);
    }
    #endregion

    #region Error
    public static void LogError(object message)
    {
        UnityEngine.Debug.LogError(message);
    }

    public static void LogError(object message, UnityEngine.Object context)
    {
        UnityEngine.Debug.LogError(message, context);
    }

    public static void ColorLogError(object message, Color color)
    {
        string coloredMessage = message.ToString().Colored(color);
        LogError(coloredMessage);
    }

    public static void ColorLogError(object message, UnityEngine.Object context, Color color)
    {
        string coloredMessage = message.ToString().Colored(color);
        LogError(coloredMessage, context);
    }
    #endregion

    #endregion

    #region CONSOLE LOG
    public static void InitializeLogger(Action<object> logCallback)
    {
        DebugDev.logEvent = logCallback;
    }

    public static void ConsoleLog(object message)
    {
        DebugDev.logEvent(message.ToString());
    }
    #endregion
}