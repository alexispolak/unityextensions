﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NullSerializableVector3 : ISerializableVector3
{
    private static NullSerializableVector3 instance;

    private NullSerializableVector3() { }
    
    public static NullSerializableVector3 Instance {
        get
        {
            if (instance == null)
                instance = new NullSerializableVector3();
            return instance;
        }
    }

    public bool Equal(SerializableVector3 vectorToCompare, float threshold = 0.005F)
    {
        DebugDev.LogError("EQUAL() --> SerializableVector3 is NULL".Bold());
        return false;
    }

    public float Magnitude()
    {
        DebugDev.LogError("Magnitude() --> SerializableVector3 is NULL".Bold());
        return -1;
    }

    public void Normalize()
    {
        DebugDev.LogError("Normalize() --> SerializableVector3 is NULL".Bold());
    }

    public SerializableVector3 Normalized()
    {
        DebugDev.LogError("Normalized() --> SerializableVector3 is NULL".Bold());
        return new SerializableVector3();
    }

    public float SqrMagnitude()
    {
        DebugDev.LogError("SqrMagnitude() --> SerializableVector3 is NULL".Bold());
        return -1;
    }
}
