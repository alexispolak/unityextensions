﻿using System;

[System.Serializable]
public class SerializableVector3 : ISerializableVector3
{
    #region PROPERTIES
    public float x { set; get; }
    public float y { set; get; }
    public float z { set; get; }
    #endregion

    #region STATIC PROPERTIES
    public static SerializableVector3 up { get => new SerializableVector3(0, 1, 0); }
    public static SerializableVector3 down { get => new SerializableVector3(0, -1, 0); }
    public static SerializableVector3 forward { get => new SerializableVector3(0, 0, 1); }
    public static SerializableVector3 back { get => new SerializableVector3(0, 0, -1); }
    public static SerializableVector3 right { get => new SerializableVector3(1, 0, 0); }
    public static SerializableVector3 left { get => new SerializableVector3(-1, 0, 0); }
    public static SerializableVector3 one { get => new SerializableVector3(1, 1, 1); }
    public static SerializableVector3 zero { get => new SerializableVector3(); }
    public static SerializableVector3 negativeInfinity { get => new SerializableVector3(float.NegativeInfinity, float.NegativeInfinity, float.NegativeInfinity); }
    public static SerializableVector3 positiveInfinity { get => new SerializableVector3(float.PositiveInfinity, float.PositiveInfinity, float.PositiveInfinity); }

    #endregion

    #region CONSTRUCTORS
    public SerializableVector3()
    {
        this.x = 0;
        this.y = 0;
        this.z = 0;
    }

    public SerializableVector3(float x, float y)
    {
        this.x = x;
        this.y = y;
        this.z = 0;
    }

    public SerializableVector3(float x, float y, float z)
    {
        this.x = x;
        this.y = y;
        this.z = z;
    }
    #endregion

    #region METHODS
    /// <summary>
    /// Returns the magnitude without the square root calculus. It's a faster way to calculate the Magnitude but it's less precise.
    /// </summary>
    public float SqrMagnitude()
    {
        return ((this.x * this.x) + (this.y * this.y) + (this.z * this.z));
    }

    /// <summary>
    /// Returns the magnitude of the vector
    /// </summary>
    public float Magnitude()
    {
        return (float)Math.Sqrt(this.SqrMagnitude());
    }

    /// <summary>
    /// Returns a new vector normalized.
    /// </summary>
    public SerializableVector3 Normalized()
    {
        var magnitude = this.Magnitude();
        return new SerializableVector3(this.x / magnitude, this.y / magnitude, this.z / magnitude);
    }

    /// <summary>
    /// Normalize the vector.
    /// </summary>
    public void Normalize()
    {
        var magnitude = this.Magnitude();
        this.x = this.x / magnitude;
        this.y = this.y / magnitude;
        this.z = this.z / magnitude;
    }

    /// <summary>
    /// Returns if the vector is equal to the received by parameter based on a threshold.
    /// </summary>
    /// <param name="vectorToCompare"> The vector that is going to be compared. </param>
    /// <param name="threshold"> The threshold of the comparison. </param>
    /// <returns></returns>
    public bool Equal(SerializableVector3 vectorToCompare, float threshold = 0.005f)
    {
        var neg = -1;
        var x = this.x - vectorToCompare.x;
        var y = this.y - vectorToCompare.y;
        var z = this.z - vectorToCompare.z;

        return (x <= threshold && x >= threshold * neg) &&
               (y <= threshold && y >= threshold * neg) &&
               (z <= threshold && z >= threshold * neg);
    }

    /// <summary>
    /// Returns a string representation of the object.
    /// </summary>
    /// <returns></returns>
    public override string ToString()
    {
        return $"({this.x},{this.y},{this.z})";
    }
    #endregion

    #region OVERLOAD
    public static SerializableVector3 operator +(SerializableVector3 a, SerializableVector3 b)
    {
        return new SerializableVector3(a.x + b.x, a.y + b.y, a.z + b.z);
    }

    public static SerializableVector3 operator -(SerializableVector3 a, SerializableVector3 b)
    {
        return new SerializableVector3(a.x - b.x, a.y - b.y, a.z - b.z);
    }

    public static SerializableVector3 operator *(SerializableVector3 a, float d)
    {
        return new SerializableVector3(a.x * d, a.y * d, a.z * d);
    }

    public static SerializableVector3 operator /(SerializableVector3 a, float d)
    {
        return new SerializableVector3(a.x / d, a.y / d, a.z / d);
    }

    public static bool operator ==(SerializableVector3 a, SerializableVector3 b)
    {
        return ((a.x == b.x) && (a.y == b.y) && (a.z == b.z));
    }

    public static bool operator !=(SerializableVector3 a, SerializableVector3 b)
    {
        return ((a.x != b.x) || (a.y != b.y) || (a.z != b.z));
    }

    #endregion
}
