﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NUnit.Framework;

public class SerializableVector3Test
{
    [Test]
    public void returns_zero_vector()
    {
        var zeroVector = SerializableVector3.zero;
        Assert.AreEqual(0, zeroVector.x);
        Assert.AreEqual(0, zeroVector.y);
        Assert.AreEqual(0, zeroVector.z);
    }

    [Test]
    public void returns_identity_vector()
    {
        var oneVector = SerializableVector3.one;
        Assert.AreEqual(1, oneVector.x);
        Assert.AreEqual(1, oneVector.y);
        Assert.AreEqual(1, oneVector.z);
    }

    [Test]
    public void returns_sqr_magnitude_of_the_vector()
    {
        Assert.AreEqual(3, SerializableVector3.one.SqrMagnitude());
    }

    [Test]
    public void returns_magnitude_of_the_vector()
    {
        var vector = new SerializableVector3(3, 0, 0);
        Assert.AreEqual(3, vector.Magnitude());
    }

    [Test]
    public void returns_a_new_vector_normalized()
    {
        var vector = new SerializableVector3(3, 0, 0);
        var normalizedVector = vector.Normalized();
        Assert.AreEqual(1, normalizedVector.x);
        Assert.AreEqual(0, normalizedVector.y);
        Assert.AreEqual(0, normalizedVector.z);
    }

    [Test]
    public void normalize_the_vector()
    {
        var vector = new SerializableVector3(3, 0, 0);
        vector.Normalize();
        Assert.AreEqual(1, vector.x);
        Assert.AreEqual(0, vector.y);
        Assert.AreEqual(0, vector.z);
    }

    [Test]
    public void vector_is_equal_when_differences_is_lower_than_threshold()
    {
        var vectorA = new SerializableVector3(0.1f, 0.10f, 0.100f);
        var vectorB = new SerializableVector3(0.2f, 0.11f, 0.101f);

        Assert.IsTrue(vectorA.Equal(vectorB, 0.1f));
    }

    [Test]
    public void vector_is_different_when_differences_is_higher_than_threshold()
    {
        var vectorA = new SerializableVector3(0.1f, 0.10f, 0.100f);
        var vectorB = new SerializableVector3(0.2f, 0.11f, 0.101f);

        Assert.IsFalse(vectorA.Equal(vectorB, 0.0005f));
    }

    [Test]
    public void addition_operator()
    {
        var vectorA = SerializableVector3.one;
        var vectorB = SerializableVector3.one;
        var addition = vectorA + vectorB;
        Assert.AreEqual(2f, addition.x, 0.00000001f);
        Assert.AreEqual(2f, addition.y, 0.00000001f);
        Assert.AreEqual(2f, addition.z, 0.00000001f);
    }

    [Test]
    public void subtraction_operator()
    {
        var vectorA = SerializableVector3.one;
        var vectorB = SerializableVector3.one;
        var subtraction = vectorA - vectorB;
        Assert.AreEqual(0, subtraction.x, 0.00000001f);
        Assert.AreEqual(0, subtraction.y, 0.00000001f);
        Assert.AreEqual(0, subtraction.z, 0.00000001f);
    }

    [Test]
    public void multiplication_operator()
    {
        var vectorA = SerializableVector3.one;
        var multiplication = vectorA * 2;
        Assert.AreEqual(2, multiplication.x, 0.00000001f);
        Assert.AreEqual(2, multiplication.y, 0.00000001f);
        Assert.AreEqual(2, multiplication.z, 0.00000001f);
    }

    [Test]
    public void divition_operator()
    {
        var vectorA = SerializableVector3.one;
        var multiplication = vectorA /2;
        Assert.AreEqual(0.5f, multiplication.x, 0.00000001f);
        Assert.AreEqual(0.5f, multiplication.y, 0.00000001f);
        Assert.AreEqual(0.5f, multiplication.z, 0.00000001f);
    }


    [Test]
    public void equal_operator()
    {
        var vectorA = SerializableVector3.one;
        var vectorB = SerializableVector3.one;
        Assert.IsTrue(vectorA == vectorB);
    }

    [Test]
    public void different_operator()
    {
        var vectorA = SerializableVector3.one;
        var vectorB = SerializableVector3.zero;
        Assert.IsTrue(vectorA != vectorB);
    }
}
