# Unity Extensions

A collection of ExtensionMethods and useful tools for Unity.

# Extension Methods

| Class  | Extends | Extensions |
| ------ | ------  | ------     |
| ListExtension.cs            | List             | 10       |
| MonoBehaviourExtension.cs   | MonoBehaviour    | 1       |
| NavMeshPathExtension.cs     | NavMeshPath      | 1       |
| QueueExtension.cs           | Queue            | 3       |
| StackExtension.cs           | Stack            | 3       |
| StringExtension.cs          | string           | 6       |
| TransformExtension.cs       | Transform        | 3       |
| Vector3Extension.cs         | Vector3          | 3       |
| ColorExtension.cs           | Color            | 1       |

# Tools

A few tools to make things easier.

# DevLog

**Description**

Logs a message to the Unity Console. Works really well with the `StringExtension.cs` of the library.

Uses the Scripting Define Symbols to activate and deactivate all the logs if you need it. You have to add `DEV_LOG` to the configuration ([how to add it](https://docs.unity3d.com/Manual/PlatformDependentCompilation.html)) and use the `DebugDev.DevLog()` method.

At this moment, it uses `Debug.Log()` internally to show the content of the string, therefore it has the same features (*concatenation of strings, Rich Text, context argument, etc.*) and more features that `Debug.Log(` doesn't have. If you need the `Debug.Log()` documentation, you can find it [here.](https://docs.unity3d.com/ScriptReference/Debug.Log.html)

You can read more in the official DebugDev documentation <a href="https://gitlab.com/alexispolak/unityextensions/blob/master/Assets/Scripts/Utilities/DebugDev.md">HERE.</a>.     

**Examples**

```
 // Show in Unity Console the message.             
 DebugDev.Log("Hello world !");

 //Shows in Unity Console the message in color.
 DebugDev.ColorLog("Hello world !", Color.red);

 //Shows in Unity Console the message if the DEV_LOG symbol is added in the configuration.
 DebugDev.DevLog("Hello world !");

 //Shows in Unity Console the message in color if the DEV_LOG symbol is added in the configuration.
 DebugDev.DevColorLog("Hello world !", Color.red);
```

**Roadmap**

In the future, it's going to be configurable to log a message to any console by callbacks.

# SerializableVector3

**Description**

Representation of 3D vectors and points in a class that can be serialized to a database or file. It's not a replace for Vector3 struct, it's just a way to save the Vector3 data. Works really well with the `Vector3Extension.cs` of the library.

It has the most important functionality of the Vector3 struct (*default vectors, SqrMagnitude(), Magnitude(), operator overload, Normalize(), Equal(), etc*) and you can transform the Vector3 into a SerializableVector3 and vice versa using the `Vector3Extension.cs` extension methods.

You can read more in the official SerializableVector3 documentation  <a href="https://gitlab.com/alexispolak/unityextensions/blob/master/Assets/Scripts/Utilities/SerializableVector3.md">HERE</a>.

**Examples**

```
// Creates a new SerializableVector3.             
SerializableVector3 serializableVector3 = new SerializableVector3();

// Creates a new SerializableVector3 with values.             
SerializableVector3 serializableVector3 = new SerializableVector3(1,2,3);

// Creates a new SerializableVector3 pointing forward (0,0,1).             
SerializableVector3 serializableVector3 = SerializableVector3.forward;

// Creates a new SerializableVector3 with identity values (1,1,1).             
SerializableVector3 serializableVector3 = SerializableVector3.one;
```



# Colaborators
@MWinocur







